# -*- coding: utf-8 -*-
"""
Test Script for Datasets
"""

import sys
import protoboard.common.datasets as pbdata

pbdata.show_datasets(verbose=1)

def test_load_data(path, plot=False):
    my_data = pbdata.Data(path)
    my_data.show_info()

if __name__ == "__main__":
    arguments = sys.argv
    if len(arguments) > 1:
        test_load_data(arguments[1], plot=True)
    else:
        print("[ERROR] No dataset was given.")

