# -*- coding: utf-8 -*-
"""
Test Script for Activation Functions
"""

import numpy as np
import protoboard.common.activation as afuncs

import matplotlib.pyplot as plt

def asymptotes(extremum):
    activations_functions = afuncs.__all__
    for f in activations_functions:
        # Get asymptotes
        y_neg = eval("afuncs.{}({})".format(f, -extremum))
        y_pos = eval("afuncs.{}({})".format(f, extremum))
        y_neg = "None" if y_neg < -10.0 else "{:.4f}".format(y_neg)
        y_pos = "None" if y_pos > 10.0 else "{:.4f}".format(y_pos)
        print("{} asymptotes = {} , {}".format(f, y_neg, y_pos))

def plot_activation_functions():
    """
    Get and evaluate the activation functions, and plot their responses.

    To Do:
    - Add example of leaky_relu using relu:
      y_leaky_relu = afuncs.relu(x, b=0.1)
    """
    x = np.linspace(-10.0, 10.0, 201)   # Make it odd to include the zero
    x_str = "np.linspace({}, {}, {})".format(-10.0, 10.0, 201)
    activations = afuncs.__all__
    for a in activations:
        y = eval("afuncs.{}({})".format(a, x_str))
        plt.plot(x, y)
    # Add plotting properties
    plt.legend(activations, loc='upper left')
    plt.grid(True)
    plt.ylim([-2, 5])
    plt.show()


if __name__ == "__main__":
    asymptotes(500.0)
    plot_activation_functions()

