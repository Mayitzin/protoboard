# -*- coding: utf-8 -*-
"""
Test Script for Computer Vision
"""

import sys
import numpy as np
import protoboard.vision.transformations as tf
from protoboard.common import mathfuncs

TOLERANCE = 1e-10
MAX_LINE_LENGTH = 70

def print_results(attribute, result):
    result_text = " OK " if result else "FAIL"
    attr_length = MAX_LINE_LENGTH - len(result_text) - 5
    print("  {:.<{width}} [{}]".format(attribute+" ", result_text, width=attr_length))

def test_Homography(num_samples=None, tolerance=TOLERANCE, verbose=0):
    if (num_samples is None) or (num_samples < 4):
        num_samples = 4
    # Generate samples
    X1 = np.random.random((2, num_samples))
    H = np.random.random((3, 3))
    # Normalize inputs
    H /= H[2, 2]
    X1 = np.r_[X1, [np.ones(num_samples)]]
    # Transform points (x' = Hx)
    X2 = np.dot(H, X1)
    # Recover Homography matrix using DLT
    H_r = tf.dlt(X1, X2)
    err = mathfuncs.matrix_diff(H, H_r)
    # Show results
    print_results("Direct Linear Transformation", result=(err < tolerance))
    if verbose:
        if verbose > 1:
            print("      sample points = {}".format(num_samples))
            print("      trace(H)   = {}".format(np.trace(H)))
            print("      trace(H_r) = {}".format(np.trace(H_r)))
        print("    DLT error = {}".format(err))

def test_rotations(num_rotations=None, tolerance=TOLERANCE, verbose=0):
    correct_rotation = True
    if num_rotations is None:
        num_rotations = 1
    angles = np.random.uniform(low=-180.0, high=180.0, size=num_rotations)
    for a in angles:
        R = tf.rotation_matrix(a)
        det_R = np.linalg.det(R)
        correct_rotation &= (abs(det_R - 1.0) < tolerance)
    print_results("Rotation construction", result=correct_rotation)

def print_header(title=""):
    h_line = '{:-<{width}}'.format('', width=MAX_LINE_LENGTH)
    print("\nTESTING: {}\n{}".format(title, h_line))

def run_tests(verbose=0):
    print_header("Computer Vision routines")
    # Test Rotations
    test_rotations(5)
    # Test Homography
    num_samples = 30
    test_Homography(num_samples, verbose=verbose)

def read_verbose(args):
    verbose = 0
    for arg in args:
        if "verbose" in arg:
            try:
                v_str = arg.split("=")[-1] if "=" in arg else args[args.index(arg)+1]
                verbose = int(v_str)
            except:
                verbose = 0
    return verbose

if __name__ == "__main__":
    arguments = sys.argv
    v = read_verbose(arguments)
    run_tests(verbose=v)
