# -*- coding: utf-8 -*-
"""
Test all modules of Package
"""

import test_cv, test_motion, test_datasets, test_activations

MAX_LINE_LENGTH = 70

H_line = '{:=<{width}}'.format('', width=MAX_LINE_LENGTH)
h_line = '{:-<{width}}'.format('', width=MAX_LINE_LENGTH)

print("\n{}".format(H_line))

test_cv.run_tests()
test_motion.run_tests()
# test_datasets.run_tests()
test_activations.plot_activation_functions()

print("\n{}".format(h_line))
