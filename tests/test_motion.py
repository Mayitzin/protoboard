# -*- coding: utf-8 -*-
"""
Test Script for Motion
"""

import sys
import numpy as np
from protoboard.motion import ahrs
from protoboard.common import mathfuncs
import random

TOLERANCE = 1e-10
MAX_LINE_LENGTH = 70

def print_results(attribute, result):
    result_text = " OK " if result else "FAIL"
    attr_length = MAX_LINE_LENGTH - len(result_text) - 5
    print("  {:.<{width}} [{}]".format(attribute+" ", result_text, width=attr_length))

def print_header(title=""):
    h_line = '{:-<{width}}'.format('', width=MAX_LINE_LENGTH)
    print("\nTESTING: {}\n{}".format(title, h_line))

def test_rotations(num_rotations=None, tolerance=TOLERANCE, verbose=0):
    possible_rotations = "xyzXYZ"
    maximum_rotations = 10
    # Set number of rotations
    if num_rotations is None:
        num_rotations = random.randint(1, maximum_rotations)
    # Generate rotation order and angles
    axis_order = "".join(random.choices(possible_rotations, k=num_rotations))
    angles = np.random.uniform(low=-180.0, high=180.0, size=num_rotations)
    # Get Rotation matrix
    R = ahrs.rot_chain(axis_order, angles)
    # Compute transposed and inversed R
    transposed_R = R.T
    inversed_R = np.linalg.inv(R)
    # Estimate Error
    det_R = np.linalg.det(R)
    err = abs(det_R - 1.0)
    # Show results
    d_error = {}
    print_results("Rotation matrices multiplication", result=(err < tolerance))
    if verbose:
        if verbose > 1:
            print("      Number of rotations = {}".format(num_rotations))
            rot_strs = []
            for i in range(num_rotations):
                rot_strs.append("R_{}({:.2f})".format(axis_order[i].lower(), angles[i]))
            rot_strs = " ".join(rot_strs)
            print("      {}".format(rot_strs))
        print("    det(R) = {}".format(det_R))
    # Difference between R*R^T and identity matrix
    matrix_diff = np.sum(abs(np.dot(R, R.T) - np.identity(3)))
    print_results("R*R^T is equal to identity matrix I_3", result=(matrix_diff < tolerance))
    if verbose:
        print("    sum(|R*R^T - I_3|) = {:.4e}".format(matrix_diff))
    # Difference between R^T and R^-1
    matrix_diff = np.sum(abs(transposed_R - inversed_R))
    print_results("R^T is equal to R^-1", result=(matrix_diff < tolerance))
    if verbose:
        print("    sum(|R^T - R^-1|) = {:.4e}".format(matrix_diff))
    # Check Orthogonalities
    ch_dist = mathfuncs.d_chord(np.dot(R, transposed_R), np.identity(3))
    print_results("Chordal distance between R*R^T and I_3", result=(ch_dist < tolerance))
    if verbose:
        print("    Chordal_distance(R*R^T, I_3) = {:.4e}".format(ch_dist))
    ch_dist = mathfuncs.d_chord(transposed_R, inversed_R)
    print_results("Chordal distance between R^T and R^-1", result=(ch_dist < tolerance))
    if verbose:
        print("    Chordal_distance(R^T, R^-1) = {:.4e}".format(ch_dist))
    # show_results(d_error)

def test_quaternions(tolerance=TOLERANCE, verbose=0):
    q = 2.0*np.random.random(4) - 1.0   # 4 Random numbers between -1.0 and 1.0
    q_norm = ahrs.qNorm(q)
    print_results("Normalization of Quaternion", result=(abs(np.linalg.norm(q_norm) - 1.0) < tolerance))
    # Compute Rotation matrix
    R = ahrs.q2R(q_norm)
    matrix_diff = np.sum(abs(np.dot(R, R.T) - np.identity(3)))
    print_results("Rotation matrix from Quaternion", result=(matrix_diff < tolerance))
    # Retrieve Quaternion back
    q_2 = ahrs.R2q(R)
    # print("q_2 =", q_2)
    # print("q_norm =", q_norm)
    print_results("Quaternion from Rotation matrix", result=(sum(abs(q_2-q_norm))<tolerance))
    # Quaternion from acceleration
    g = np.array([0.0, 0.0, 9.81])  # Gravity along Z-axis
    q_null = np.array([1., 0., 0., 0.])
    R = ahrs.q2R(q_null)
    a = np.dot(R.transpose(), g.transpose())
    # print(q_null)
    # print(ahrs.am2q(a))
    print_results("Quaternion from Acceleration", result=(sum(abs(q_null-ahrs.am2q(g)))<tolerance))
    # Axis-Angle to Quaternion
    axis, angle = ahrs.quat2axang(q_norm)
    q_3 = ahrs.axang2quat(axis, angle)
    print_results("Quaternion from Axis-Angle", result=(sum(abs(q_3-q_norm))<tolerance))

def show_results(d=None):
    for k in list(d.keys()):
        print_results(k, d[k]["result"])
        if "v1" in list(d[k].keys()):
            print("    {}".format(d[k]["v1"]))

def run_tests(verbose=0):
    print_header("Motion routines")
    test_quaternions(verbose=verbose)
    test_rotations(num_rotations=5, verbose=verbose)

def read_verbose(args):
    verbose = 0
    for arg in args:
        if "verbose" in arg:
            try:
                v_str = arg.split("=")[-1] if "=" in arg else args[args.index(arg)+1]
                verbose = int(v_str)
            except:
                verbose = 0
    return verbose

if __name__ == "__main__":
    arguments = sys.argv
    v = read_verbose(arguments)
    run_tests(verbose=v)
