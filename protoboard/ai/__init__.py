# -*- coding: utf-8 -*-
"""
Intelligence
============

References
----------

.. [HZ] Hartley, R., & Zisserman, A. (2004). Multiple View Geometry in Computer
        Vision. Cambridge University Press.
        (http://www.robots.ox.ac.uk/~vgg/hzbook/)
.. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
        Cambridge University Press. (http://www.computervisionmodels.com/)
.. [RW] Rasmussen, C. E. et al. (2006) Gaussian Processes for Machine Learning.
        MIT Press. (http://www.gaussianprocess.org/gpml/)

"""

from . import learn

def print_module_name():
    print("Module: Intelligence")
