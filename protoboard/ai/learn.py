# -*- coding: utf-8 -*-
"""
Statistical Learning routines
=============================

References
----------
.. [DB] Barber, D. (2017) Bayesian Reasoning and Machine Learning.
        (http://web4.cs.ucl.ac.uk/staff/D.Barber/pmwiki/pmwiki.php?n=Brml.HomePage)
.. [CB] Bishop, C. (2006) Pattern Recognition and Machine Learning. Springer.
        (https://www.microsoft.com/en-us/research/people/cmbishop/#!prml-book)

"""

import numpy as np
import scipy.special as sp
from protoboard.common.mathfuncs import *

def mle_norm(x):
    """
    Maximum Likelihood estimation of normal distribution.

    *** TO BE IMPLEMENTED ***

    Find the set of parameters {mu, sigma} under which the given data `x` are
    most likely, assuming each data point was drawn independently from the
    distribution.

    Parameters
    ----------
    x : float or ndarray
        Training data.

    Returns
    -------
    mu : float or ndarray
        Mean of the distribution.
    sigma : float or ndarray
        Variance of the distribution.

    References
    ----------
    .. [DB] Barber, D. (2017) Bayesian Reasoning and Machine Learning. Pages
            188-189.
    """
    return None

def map_norm(x, a, b, c, d):
    """
    Maximum A-Posteriori estimation of univariate normal parameters.

    *** TO BE IMPLEMENTED ***

    MAP maximizes the posterior probability p(mu, sigma | x) of the parameters,
    given prior information about the parameters.

    Parameters
    ----------
    x : ndarray
        Training data.
    """
    return None

def mog_fit(x, K):
    """
    Return the Mixture of Gaussians.

    *** TO BE IMPLEMENTED ***

    Estimate the complex multimodal probabilty density function from taking a
    weighted sum of `K` normal distributions with different means and variances.

    References
    ----------
    .. [CB] Bishop, C. (2006) Pattern Recognition and Machine Learning.
            Springer. Pages 110-113 and 430-439.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Pages 108-115.
    """
    return None

def t_dist_fit(x):
    """
    Estimate t-distribution parameters of a given data `x`.

    *** TO BE IMPLEMENTED ***

    Use the EM algorithm to fit the set of parameters {Mu, Sigma, v} under
    which the given data `x` are most likely.

    Parameters
    ----------
    x : ndarray
        Training data.

    Returns
    -------
    Mu : float or ndarray
        Mean of the distribution.
    Sigma : float or ndarray
        Variance of the distribution.
    v : float

    References
    ----------
    .. [CB] Bishop, C. (2006) Pattern Recognition and Machine Learning.
            Springer. Pages 102-105 and 691-692.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Pages 115-120.
    """
    return None

def gp(x, y, xtest, covpr, covfn, varytrain):
    """
    Compute the Gaussian Process regression from given data `X`.

    *** TO BE IMPLEMENTED ***

    Parameters
    ----------
    x : ndarray
        matrix of training data. Each column contains a datapoint
    y : ndarray
        vector of 1 dimensional outputs corresponding to xtrain
    xtest : ndarray
        the test inputs
    covpr : ndarray
        covariance parameters
    covfn : string
        covariance function
    varytrain : float
        training output variance

    Returns
    -------
    meantest : ndarray
        mean of the clean underlying function on the test points
    vartest : ndarray
        variance of the clean underlying function on the test points
    logpygx : ndarray
        log likelihood of the training data 

    References
    ----------
    .. [DB] Barber, D. (2017) Bayesian Reasoning and Machine Learning. Pages
            399-402.
    """
    return None

def lin_reg(X):
    """
    Linear Regression with Maximum Likelihood fitting.

    *** TO BE IMPLEMENTED ***

    Parameters
    ----------
    X : ndarray
        (m+1)-by-n data matrix, where `m` is the data dimensionality and `n`
        is the number of training examples. The training examples are in X's
        columns rather than in the rows, and the first row of X equals
        ones(1,n).
    w : ndarray
        n-by-1 vector containing the target variables.

    Returns
    -------
    phi : ndarray
        (m+1)-by-1 vector containing the linear function coefficients.

    References
    ----------
    .. [CB] Bishop, C. (2006) Pattern Recognition and Machine Learning.
            Springer. Pages 102-105 and 691-692.

    """
    return None

def k_means(X, K, D):
    """
    Identify the centroids of the given data points.

    *** TO BE IMPLEMENTED ***

    Parameters
    ----------
    X : ndarray
        Set of data
    K : int
        Number of clusters
    D : int
        Data dimension

    Returns
    -------
    y : ndarray
        Centroids of each cluster.

    References
    ----------
    .. [CB] Bishop, C. (2006) Pattern Recognition and Machine Learning.
            Springer. Pages 424-428.
    """
    return None

class kernel:
    """
    Kernel functions.

    References
    ----------
    .. [CR] Rasmussen, C.E. (2016) Gaussian Process Covariance Functions.
            (http://mlg.eng.cam.ac.uk/teaching/4f13/1819/covariance%20functions.pdf)
    .. [MG] Genton, M. (2001) Classes of Kernels for Machine Learning: A
            Statistics Perspective. Journal of Machine Learning Research.
            (http://www.jmlr.org/papers/volume2/genton01a/genton01a.pdf)
    .. [CB] Bishop, C. (2006) Pattern Recognition and Machine Learning.
            Springer. Pages 291-303.
    .. [AW] Wilson, A. G. et al. (2013) Gaussian Process Kernels for Pattern
            Discovery and Extrapolation. (https://arxiv.org/pdf/1302.4245.pdf)

    See Also
    --------
    - https://www.cs.toronto.edu/~duvenaud/cookbook/
    - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/
    """
    @staticmethod
    def cauchy(x, y, sigma=1.0):
        """
        Cauchy kernel

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/
        """
        if np.shape(x) == np.shape(y):
            c = abs(x-y)**2/sigma**2
            return 1.0/(1.0+c)

    @staticmethod
    def chi_square(x, y):
        """
        Chi-Square kernel

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/
        """
        if np.shape(x) == np.shape(y):
            k = 0.0
            N = len(x)
            for i in range(N):
                k += (2.0*x[i]*y[i])/(x[i]+y[i])
            return k

    @staticmethod
    def circular(x, y, sigma=1.0):
        """
        Circular kernel

        .. math::

            k(x, y) = \\left\\{
            \\begin{array}{lr}
                \\frac{2}{\\pi} \\mathrm{acos} (-r)-\\frac{2}{\\pi}r\\sqrt{1-r^2} & \\: \\mathrm{if} \\|x-y\\| < \\sigma \\\\
                0 & \\: \\mathrm{otherwise}
            \\end{array}
            \\right.

        where:

        .. math::

            r = \\frac{\\|x-y\\|}{\\sigma}

        References
        ----------
        .. [MG] Genton, M. (2001) Classes of Kernels for Machine Learning: A
                Statistics Perspective. Journal of Machine Learning Research.
                (http://www.jmlr.org/papers/volume2/genton01a/genton01a.pdf)
        .. [SB] Boughorbel, S. et al. (2005) GCS Kernel For SVM-Based Image
                Recognition. (http://perso.lcpc.fr/tarel.jean-philippe/publis/jpt-icann05b.pdf)

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#circular
        """
        if np.shape(x) == np.shape(y):
            if abs(x-y) < sigma:
                _2pi = 2.0/np.pi
                r = abs(x-y)/sigma
                n1 = _2pi*(np.arccos(-r))
                n2 = _2pi*r*np.sqrt(1.0-r**2)
                return n1-n2
            else:
                return 0.0

    @staticmethod
    def exponential(x, y, sigma=1.0):
        """
        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#exponential
        """
        if np.shape(x) == np.shape(y):
            gamma = 2.0*sigma**2
            return np.exp(-abs(x-y)/gamma)

    @staticmethod
    def gaussian(x, y, sigma=1.0):
        """
        See Also
        --------
        - https://en.wikipedia.org/wiki/Radial_basis_function_kernel
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#gaussian
        """
        if np.shape(x) == np.shape(y):
            gamma = 2.0*sigma**2
            return np.exp(-abs(x-y)**2/gamma)

    @staticmethod
    def gen_t_student(x, y, d=1.0):
        """
        Generalized T-Student kernel

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/
        """
        if np.shape(x) == np.shape(y):
            return 1.0 / (1.0-abs(x-y)**d)

    @staticmethod
    def inv_multiquadratic(x, y):
        """
        Inverse Multiquadratic kernel

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#inverse_multiquadric
        """
        if np.shape(x) == np.shape(y):
            return 1.0/kernel.multiquadratic(x, y)

    @staticmethod
    def laplacian(x, y, sigma=1.0):
        """
        Laplacian kernel

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#laplacian
        """
        if np.shape(x) == np.shape(y):
            return np.exp(-abs(x-y)/sigma)

    @staticmethod
    def linear(x, y):
        """
        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#linear
        """
        if np.shape(x) == np.shape(y):
            return kernel.polynomial(x, y)

    @staticmethod
    def log(x, y, d=1.0):
        """
        Rational Quadratic kernel

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#log
        """
        if np.shape(x) == np.shape(y):
            return -np.log(abs(x-y)**d+1.0)

    @staticmethod
    def matern(x, y, v=None, sigma=1.0):
        """
        Matérn kernel

        The Matérn kernel is defined [MG]_ as:

        .. math::

            k_v(x, y) = \\frac{2^{1-v}}{\\Gamma(v)} \\big(\\sqrt{2v}r\\big)^v K_v(\\sqrt{2v}r)

        where:

        .. math::

            r = \\frac{\\|x-y\\|}{\\sigma}

        :math:`\\Gamma` is the gamma function, :math:`K_v` is the
        modified Bessel function of second kind of order :math:`v`, and
        :math:`\\sigma` is the characteristic length scale.

        Matérn forms are :math:`\\lceil v-1\\rceil` times differentiable,
        making :math:`v` a control degree of the smoothness.

        Special cases are:

        Matérn :math:`v=3/2` :

        .. math::

            k_{3/2}(x, y) = (1 + \\sqrt{3}r) e^{-\\sqrt{3}r}

        Matérn :math:`v=5/2` :

        .. math::

            k_{5/2}(x, y) = (1 + \\sqrt{5}r + \\frac{5}{3}r^2) e^{-\\sqrt{5}r}

        Parameters
        ----------
        x : ndarray
            First dimension values of input space.
        y : ndarray
            Second dimension values of input space.
        v : int or string
            Order of the modified Bessel function of second kind. Controls the
            smoothness of the resulting function.

        References
        ----------
        .. [MG] Genton, M. (2001) Classes of Kernels for Machine Learning: A
                Statistics Perspective. Journal of Machine Learning Research.
                (http://www.jmlr.org/papers/volume2/genton01a/genton01a.pdf)
        .. [CR] Rasmussen, C.E. (2016) Gaussian Process Covariance Functions.
                (http://mlg.eng.cam.ac.uk/teaching/4f13/1819/covariance%20functions.pdf)

        """
        if np.shape(x) == np.shape(y):
            # Identify the form of the kernel
            if v is None:
                v = 0.5         # Equivalent to Matérn 1/2
            r = abs(x-y)/sigma
            if v==1/2:
                return np.exp(-r)
            if v==3/2:
                return (1.0+np.sqrt(3.0)*r)*np.exp(-np.sqrt(3.0)*r)
            if v==5/2:
                return (1.0+np.sqrt(5.0)*r+(5.0/3.0)*r**2) * np.exp(-np.sqrt(5.0)*r)
            else:
                return ((2**(1-v))/sp.gamma(v))*(np.sqrt(2.0*v)*r)**v*sp.kv(np.sqrt(2.0*v)*r)

    @staticmethod
    def mexican_hat(x, y, sigma=1.0):
        """
        Mexican hat wavelet kernel

        Parameters
        ----------
        x : ndarray
            First dimension values of input space.
        y : ndarray
            Second dimension values of input space.
        sigma : float
            length scale.

        References
        ----------
        .. [LZ] Zhang, L. (2004) Wavelet Support Vector Machine.
                (http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.412.362&rep=rep1&type=pdf)
        .. [SH] Hou, S. (2017) Wavelet Support Vector Machine Algorithm in
                Power Analysis Attacks.
                (https://www.radioeng.cz/fulltexts/2017/17_03_0890_0902.pdf)

        """
        if np.shape(x) == np.shape(y):
            N = len(x)
            k = 1.0
            for i in range(N):
                k *= (1.0-(x[i]-y[i])**2/(sigma**2))*np.exp(-abs(x[i]-y[i])**2/(2.0*sigma**2))
            return k

    @staticmethod
    def multiquadratic(x, y):
        """
        Multiquadratic kernel

        .. math::

            k(x, y) = \\sqrt{\\|x-y\\|^2 +c^2}

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#multiquadric
        """
        if np.shape(x) == np.shape(y):
            return np.sqrt(abs(x-y)**2)

    @staticmethod
    def periodic(x, y, sigma=1.0):
        """
        Periodic kernel

        References
        ----------
        .. [CR] Rasmussen, C.E. (2016) Gaussian Process Covariance Functions.
                (http://mlg.eng.cam.ac.uk/teaching/4f13/1819/covariance%20functions.pdf)
        """
        if np.shape(x) == np.shape(y):
            return np.exp(-2.0*np.sin(np.pi*(x-y))**2/sigma**2)

    @staticmethod
    def polynomial(x, y, alpha=1.0, d=1.0):
        """
        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#polynomial
        """
        if np.shape(x) == np.shape(y):
            return np.dot(alpha*x.flatten(), y.flatten())**d

    @staticmethod
    def power(x, y, d=1.0):
        """
        Rational Quadratic kernel

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#power

        """
        if np.shape(x) == np.shape(y):
            return -abs(x-y)**d

    @staticmethod
    def ratquat(x, y, alpha=1.0, sigma=1.0):
        """
        Rational Quadratic kernel

        ToDo:
        - Compare it to the kernel defined in [MG] and [CR]

        See Also
        --------
        - https://www.cs.toronto.edu/~duvenaud/cookbook/
        """
        if np.shape(x) == np.shape(y):
            return sigma**2.0*(1.0+(abs(x-y)**2/(2.0*alpha*s**2)))**-alpha

    @staticmethod
    def rbf(x, y, sigma=1.0):
        """
        Radial Basis Function kernel. Synonym of the gaussian kernel

        See Also
        --------
        - https://en.wikipedia.org/wiki/Radial_basis_function_kernel
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#gaussian
        """
        if np.shape(x) == np.shape(y):
            return kernel.gaussian(x, y, sigma=sigma)

    @staticmethod
    def spherical(x, y, sigma=1.0):
        """
        Spherical kernel

        References
        ----------
        .. [MG] Genton, M. (2001) Classes of Kernels for Machine Learning: A
                Statistics Perspective. Journal of Machine Learning Research.
                (http://www.jmlr.org/papers/volume2/genton01a/genton01a.pdf)

        """
        if np.shape(x) == np.shape(y):
            if abs(x-y) < sigma:
                r = abs(x-y)/sigma
                return 1.0 - 1.5*r + 0.5*r**3
            else:
                return 0.0

    @staticmethod
    def tanh(x, y, alpha=1.0):
        """
        Hyperbolic Tangent kernel

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/#sigmoid
        """
        if np.shape(x) == np.shape(y):
            return tanh(alpha*np.dot(x.T, y))

    @staticmethod
    def wave(x, y, sigma=1.0):
        """
        Wave kernel

        References
        ----------
        .. [MG] Genton, M. (2001) Classes of Kernels for Machine Learning: A
                Statistics Perspective. Journal of Machine Learning Research.
                (http://www.jmlr.org/papers/volume2/genton01a/genton01a.pdf)

        """
        if np.shape(x) == np.shape(y):
            diff = abs(x-y)
            return (sigma/diff)*np.sin(diff/sigma)

    @staticmethod
    def wavelet(x, y, a=1.0, c=0.0):
        """
        Wavelet kernel

        Parameters
        ----------
        x : ndarray
            First dimension values of input space.
        y : ndarray
            Second dimension values of input space.
        a : float
            Dilation coefficient.
        c : float
            Translation coefficient.

        References
        ----------
        .. [LZ] Zhang, L. (2004) Wavelet Support Vector Machine.
                (http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.412.362&rep=rep1&type=pdf)

        See Also
        --------
        - http://crsouza.com/2010/03/17/kernel-functions-for-machine-learning-applications/
        """
        if np.shape(x) == np.shape(y):
            h = lambda z : np.cos(1.75*z)*np.exp(-z**2/2.0)     # Morlet wavelet function
            N = len(x)
            k = 1.0
            if c == 0.0:
                for i in range(N):
                    k *= h((x[i]-y[i])/a)
            else:
                for i in range(N):
                    k *= h((x[i]-c)/a)*h((y[i]-c)/a)
            return k


