# -*- coding: utf-8 -*-
"""
Feature Tracking
================

References
----------
.. [ND] Hartley, R. et al. (2011) Multiple View Geometry. Cambridge University
        Press. (http://www.robots.ox.ac.uk/~vgg/hzbook/)
.. [RS] Silezki, R. (2010) Computer Vision: Algorithms and Applications.
        Springer. (http://szeliski.org/Book/)
.. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
        Cambridge University Press. (http://www.computervisionmodels.com/)

"""

__all__ = []

import numpy as np

def lucas_kanade(x):
    """
    Estimate the motion of the given features using the Lucas-Kanade method.

    *** TO BE IMPLEMENTED ***

    Archetypal implementation of the feature tracking method proposed by Bruce
    Lucas and Tadeo Kanade.

    References
    ----------
    .. [LK] Lucas, B. et al. (1981) An Iterative Image Registration Technique
            with an Application to Stereo Vision.
            (http://cseweb.ucsd.edu/classes/sp02/cse252/lucaskanade81.pdf)
    .. [RS] Silezki, R. (2010) Computer Vision: Algorithms and Applications.
            Springer. Pages 409-410.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Page 339.

    See Also
    --------
    - https://en.wikipedia.org/wiki/Lucas%E2%80%93Kanade_method

    """
    return None

