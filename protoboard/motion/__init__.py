# -*- coding: utf-8 -*-
"""
Motion
======

Kinematics
----------

+ Motion
  - Rigid body mechanics
  - Position and Orientation
  - Geometric Representation
  - Kinematic Chains
+ Forward Kinematics
+ Inverse Kinematics
+ Differential Kinematics

Dynamics
--------

+ Dynamic models of rigid-bodies
+ Attitude and Heading Reference Systems.
  - Orientation estimation.
  - Position estimation.
+ Navigation
  - Kalman Filter
  - Extended Kalman Filter

Robotics
--------

+ Mechanisms and Actuation
  - Mechanical Structure
  - Joint Mechanisms

References
----------

.. [JC] Craig, J. (2003). Introduction to Robotics: Mechanics and Control.
.. [TB] Thrun, S. (2005). Probabilistic Robotics.
        (http://www.probabilistic-robotics.org/)
.. [PC] Corke, P. (2013). Robotics, Vision and Control. Fundamental Algorithms
        in MATLAB (http://petercorke.com/wordpress/books/book)
.. [SK] Siciliano, B. (2016). Handbook of Robotics. Springer.

"""

from . import ahrs

