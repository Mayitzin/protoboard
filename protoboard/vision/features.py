# -*- coding: utf-8 -*-
"""
Feature Extraction
==================

References
----------
.. [ND] Hartley, R. et al. (2011) Multiple View Geometry. Cambridge University
        Press. (http://www.robots.ox.ac.uk/~vgg/hzbook/)
.. [RS] Silezki, R. (2010) Computer Vision: Algorithms and Applications.
        Springer. (http://szeliski.org/Book/)
.. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
        Cambridge University Press. (http://www.computervisionmodels.com/)

"""

__all__ = []

import numpy as np

def blobs(img, method=None):
    """
    Return the positions and sizes of blobs in an image.

    *** TO BE IMPLEMENTED ***

    Parameters
    ----------
    img : str or array
        File name or array of an image.
    method : str
        The mode parameter determines how the input image is processed to
        obtain the location and size of the blobs in the image. The valid
        values are:
        'LoG' Laplace of Gaussians.
        'DoG' Difference of Gaussians.
        'DetHess' Determinant of Hessian.
        'MSER' Maximally Stable Extremal Regions.
        'Marr-Hildert' the Marr-Hildert Algorithm.

    References
    ----------
    .. [TL] Lindberg, T. (1993) Detecting Salient Blob-Like Image Structures
            and Their Scales with a Method for Focus-of-Attention.
            (http://www.nada.kth.se/~tony/abstracts/Lin92-IJCV.html)

    See Also
    --------
    - https://en.wikipedia.org/wiki/Blob_detection
    - https://en.wikipedia.org/wiki/Difference_of_Gaussians
    - https://en.wikipedia.org/wiki/Marr%E2%80%93Hildreth_algorithm

    """
    return None

def bow(img):
    """
    Return the features of an image using the Bag-of-words method.

    *** TO BE IMPLEMENTED ***

    References
    ----------
    .. [RS] Silezki, R. (2010) Computer Vision: Algorithms and Applications.
            Springer. Pages 697-701.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Pages 334-335.

    See Also
    --------
    - http://people.csail.mit.edu/fergus/iccv2005/bagwords.html

    """
    return None

def canny(img):
    """
    Return the edges of an image using the Canny edge detector.

    *** TO BE IMPLEMENTED ***

    Archetypal implementation of the edge detector proposed by John Canny.

    References
    ----------
    .. [JC] Canny, J. (1986) A computational Approach to Edge Detection.
            (https://www.computer.org/cms/Computer.org/Transactions%20Home%20Pages/TPAMI/PDFs/top_ten_6.pdf)
    .. [RS] Silezki, R. (2010) Computer Vision: Algorithms and Applications.
            Springer. Pages 238-243.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Pages 336-338.

    See Also
    --------
    - Step-wise algorithm explanation by Justin Liang.
      (http://justin-liang.com/tutorials/canny/)

    """
    return None

def fast(img):
    """
    Return corners in image using Features from Accelerated Segment Test (FAST)

    References
    ----------
    .. [RD] Rosten, E. et al. (2005) Fusing Points and Lines for High
            Performance Tracking.
            (http://www.edwardrosten.com/work/rosten_2005_tracking.pdf)

    See Also
    --------
    - https://en.wikipedia.org/wiki/Features_from_accelerated_segment_test
    """

def harris(img):
    """
    Return the feature points of an image using the Harris corner detector.

    *** TO BE IMPLEMENTED ***

    Archetypal implementation of the corner detector proposed by Harris and
    Stephens.

    References
    ----------
    .. [HS] Harris, C. et al. (1988) A combined corner and edge detector.
            (http://www.bmva.org/bmvc/1988/avc-88-023.pdf)
    .. [RS] Silezki, R. (2010) Computer Vision: Algorithms and Applications.
            Springer. Pages 212-214.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Page 339.

    See Also
    --------
    - Jupyter Notebook of the Harris Corner Detector at
      https://github.com/Mayitzin/Robotics/blob/master/Sensing/Vision/AlgHarrisCorner.ipynb

    """
    return None

def sift(img):
    """
    Return the features of an image using the SIFT detector.

    *** TO BE IMPLEMENTED ***

    References
    ----------
    .. [DL] Lowe, D. (2004) Distinctive Image Features from Scale-Invariant
            Keypoints. (https://www.cs.ubc.ca/~lowe/papers/ijcv04.pdf)
    .. [RS] Silezki, R. (2010) Computer Vision: Algorithms and Applications.
            Springer. Pages 238-243.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Pages 339-343.

    See Also
    --------
    - https://en.wikipedia.org/wiki/Scale-invariant_feature_transform

    """
    return None

def surf(img):
    """
    Return the features of an image using the SURF detector.

    *** TO BE IMPLEMENTED ***

    References
    ----------
    .. [HB] Bay, H. et al. (2008) Speeded-Up Robust Features (SURF).
            (http://www.vision.ee.ethz.ch/en/publications/papers/articles/eth_biwi_00517.pdf)

    See Also
    --------
    - https://en.wikipedia.org/wiki/Speeded_up_robust_features

    """
    return None

def susan(img, corner=False):
    """
    Return the features of an image using the SUSAN feature detector.

    *** TO BE IMPLEMENTED ***

    References
    ----------
    .. [SB] Smith, S.M. et al. (1995) SUSAN - A New Approach to Low Level Image
            Processing. (http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.27.8270&rep=rep1&type=pdf)

    See Also
    --------
    - https://en.wikipedia.org/wiki/Corner_detection#The_SUSAN_corner_detector

    """
    return None


