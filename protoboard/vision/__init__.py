# -*- coding: utf-8 -*-
"""
Vision
======

Image Processing
----------------

+ Image enhancement
  - Image denoising
  - Image histogram
  - Histogram equalization
  - Tone mapping
  - Gamma correction
+ Image rectification
  - Camera Models
  - Geometric Transformations
  - Homography
  - Hough transform
  - Morphology

Image Understanding
-------------------

+ Feature Extraction
  - Edges and interest points
  - Blob detection
  - SIFT, SURF, HOGs, and more
  - Feature matching
+ Multiple Views
  - Camera Model
  - Image stitching
  - Pose Estimation
  - Optical Flow
  - Structure from Motion

References
----------

.. [HZ] Hartley, R., & Zisserman, A. (2004). Multiple View Geometry in Computer
        Vision. Cambridge University Press.
        (http://www.robots.ox.ac.uk/~vgg/hzbook/)
.. [RZ] Szeliski, R. (2010). Computer Vision: Algorithms and Applications.
        Springer. (http://szeliski.org/Book/)
.. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
        Cambridge University Press. (http://www.computervisionmodels.com/)
.. [YM] Ma, Y. (2001). An invitation to 3-D Vision. From images to geometric
        models. Springer.
.. [FP] Forsyth, D. (2002) Computer Vision: A Modern Approach.
        (http://luthuli.cs.uiuc.edu/~daf/CV2E-site/cv2eindex.html)
.. [CS] Steger, C. et al. (2008). Machine Vision Algorithms and Applications.

"""

from . import transformations
from . import features

def print_module_name():
    print("Module: Vision")
