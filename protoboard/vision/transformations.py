# -*- coding: utf-8 -*-
"""
Geometric Transformations
=========================

References
----------
.. [ND] Hartley, R. et al. (2011) Multiple View Geometry. Cambridge University
        Press. (http://www.robots.ox.ac.uk/~vgg/hzbook/)
.. [RS] Silezki, R. (2010) Computer Vision: Algorithms and Applications.
        Springer. (http://szeliski.org/Book/)
.. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
        Cambridge University Press. (http://www.computervisionmodels.com/)

"""

__all__ = ['hmtrans', 'invTrans', 'dlt', 'dlt_norm']

import numpy as np
from protoboard.common.mathfuncs import *
from protoboard.motion.ahrs import rotation

def rotation_matrix(angle):
    """
    Create a 2-by-2 rotation matrix about 'Z' through a given angle.
    """
    R = rotation(ax='Z', ang=angle)[:2,:2]
    return R

def hmtrans(R, t):
    """
    Build a 4-by-4 homogeneous transformation matrix of the form:

        T = | R t |
            | 0 1 |

    where R is a 3-by-3 rotation matrix and t is the 3-translation vector.

    """
    m, n = np.shape(R)
    if m != n:
        return None
    if m != len(t):
        return None
    Rt = np.hstack((R, t))
    # Normalize Matrix
    T = np.vstack((Rt, np.array(n*[0.0]+[1.0])))
    return T.astype(np.float)

def invTrans(T):
    """
    Compute the 4-by-4 inverse homogeneous transformation of T, whose output is
    of the form:

        T^-1 = | R^T  -R^T*t |
               |  0      1   |

    """
    R = T[0:3, 0:3]
    t = np.vstack(T[0:3, 3])
    return np.vstack((np.hstack((R.T, np.dot(-R.T, t))), np.array([0.0, 0.0, 0.0, 1.0])))

def dlt(X1, X2):
    """
    Estimate Homography using the Direct Linear Transformation.

    Parameters
    ----------
    X1 : ndarray
        2-by-N NumPy array with the coordinates of the first set of points.
    X1 : ndarray
        2-by-N NumPy array with the coordinates of the second set of points.

    References
    ----------
    .. [ND] Hartley, R. et al. (2011) Multiple View Geometry. Cambridge University
            Press. Pages 88-91.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Pages 400-401.
    """
    # Enforce NumPy array
    if type(X1) != np.ndarray:
        X1 = np.asarray(X1)
    if type(X2) != np.ndarray:
        X2 = np.asarray(X2)
    # Assert Dimensions
    m1, n1 = np.shape(X1)
    m2, n2 = np.shape(X2)
    if (m1 != m2) or (n1 != n2):
        return None
    # Transpose if points are in row-array
    if n1 < 4:
        return None
    if m1 == 2:
        # Normalize input points
        X1 = np.r_[X1, [np.ones(m)]]
        X2 = np.r_[X2, [np.ones(m)]]
    # Construction of Matrix A_i
    A = []
    for i in range(n1):
        X = X1[:, i]
        x_i, y_i, w_i = X2[:, i]
        # Create elements of matrix A_i
        A.append([0.0, 0.0, 0.0] + list(-w_i*X) + list(y_i*X))
        A.append(list(w_i*X) + [0.0, 0.0, 0.0] + list(-x_i*X))
    A = np.array(A)
    # Solutions for h to get Homography H
    U, S, V = np.linalg.svd(A)
    H = V[-1,:].reshape(3, 3)
    H /= H[2, 2]
    return H

def dlt_norm(X1, X2):
    """
    Estimate Homography using the Normalized Direct Linear Transformation.

    Parameters
    ----------
    X1 : ndarray
        2-by-N NumPy array with the coordinates of the first set of points.
    X1 : ndarray
        2-by-N NumPy array with the coordinates of the second set of points.

    References
    ----------
    .. [ND] Hartley, R. et al. (2011) Multiple View Geometry. Cambridge University
            Press. Pages 104-110.
    """
    if type(X1) != np.ndarray:
        X1 = np.asarray(X1)
    if type(X2) != np.ndarray:
        X2 = np.asarray(X2)
    m, n = np.shape(X1)
    if n < 3:
        # Normalization of points
        X1 = np.hstack((X1, np.ones((m, 1))))
        X2 = np.hstack((X2, np.ones((m, 1))))
    # Obtain centroid of points in each image
    X1_centroid = np.mean(X1, 0)
    X2_centroid = np.mean(X2, 0)
    # Average distance of the shifted points to the centroid
    avg1, avg2 = 0.0, 0.0
    for i in range(n):
        avg1 += np.sqrt((X1[i, 0]-X1_centroid[0])**2 + (X1[i, 1]-X1_centroid[1])**2)
        avg2 += np.sqrt((X2[i, 0]-X2_centroid[0])**2 + (X2[i, 1]-X2_centroid[1])**2)
    avg1 /= n
    avg2 /= n
    # Average distance to origin is = sqrt(2)
    s1 = np.sqrt(2.0) / avg1
    s2 = np.sqrt(2.0) / avg2
    # Similarity Transformations
    T = np.array([[s1, 0.0, -s1*X1_centroid[0]],
                  [0.0, s1, -s1*X1_centroid[1]],
                  [0.0, 0.0, 1.0]])
    U = np.array([[s2, 0.0, -s2*X2_centroid[0]],
                  [0.0, s2, -s2*X2_centroid[1]],
                  [0.0, 0.0, 1.0]])
    # Transformation is applied to each of the two set of points independently
    X1 = np.dot(T, X1.transpose()).transpose()
    X2 = np.dot(U, X2.transpose()).transpose()
    # Regular Computation of DLT
    Hn = dlt(X1, X2)
    # De-normalization of H
    H = np.dot(np.linalg.pinv(U), np.dot(Hn, T))
    H /= H[2, 2]
    return H

def affine(x, A=None):
    return None

def projective(x, P=None):
    return None

def polar(x, d=0.0, a=0.0):
    return None

def radial_correction():
    """
    Correct the radial distortion on an image caused by lenses.

    *** TO BE IMPLEMENTED ***

    References
    ----------
    .. [ND] Hartley, R. et al. (2011) Multiple View Geometry. Cambridge University
            Press. Pages 189-191.

    """
    return None

def cam_matrix():
    """
    Return the Camera matrix from point correspondences using the standard gold
    algorithm.

    *** TO BE IMPLEMENTED ***

    References
    ----------
    .. [ND] Hartley, R. et al. (2011) Multiple View Geometry. Cambridge University
            Press. Pages 178-181.

    """
    return None

def essential_matrix():
    """
    Return the Essential matrix.

    *** TO BE IMPLEMENTED ***

    References
    ----------
    .. [ND] Hartley, R. et al. (2011) Multiple View Geometry. Cambridge University
            Press. Pages 257-259.
    .. [RS] Silezki, R. (2010) Computer Vision: Algorithms and Applications.
            Springer. Pages 347-350.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Pages 427-431.

    """
    return None

def eight_point():
    """
    Return the Fundamental matrix estimated with the normalized Eight-Point
    algorithm.

    *** TO BE IMPLEMENTED ***

    References
    ----------
    .. [ND] Hartley, R. et al. (2011) Multiple View Geometry. Cambridge University
            Press. Pages 279-282.
    .. [RS] Silezki, R. (2010) Computer Vision: Algorithms and Applications.
            Springer. Pages 350-352.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Pages 432-435.

    """
    return None

def fundamental_matrix():
    """
    Return the Fundamental matrix estimated with the normalized Eight-Point
    algorithm and RANSAC.

    *** TO BE IMPLEMENTED ***

    References
    ----------
    .. [ND] Hartley, R. et al. (2011) Multiple View Geometry. Cambridge University
            Press. Pages 290-291.
    .. [SP] Prince, S. (2012) Computer Vision: Models, Learning, and Inference.
            Cambridge University Press. Pages 432-435.

    """
    return None
