# -*- coding: utf-8 -*-
"""
Protoboard
==========

@author: Mario Garcia

"""

from . import vision
from . import motion
from . import common

# import ..versions
# VERSION = versions.PKG_VERSION
# from ..versions import pkg_version
# VERSION = pkg_version()
VERSION = "0.0.4a2"

def print_package_name():
    print("Package: Protoboard v.{}".format(VERSION))
