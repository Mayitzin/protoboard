DATASETS = {
    "ETH-ASL-EuRoC": {
        "description" : "Visual-inertial datasets collected on-board a Micro Aerial Vehicle (MAV) at\n"
                       +"the Autonomous Systems Lab of the Swiss Federal Institute of Technology Zuerich.\n"
                       +"The datasets contain stereo images, synchronized IMU measurements, and accurate\n"
                       +"motion and structure ground-truth.",
        "headers" : {
            "IMU" : ['timestamp [ns]', 'w_RS_S_x [rad s^-1]', 'w_RS_S_y [rad s^-1]', 'w_RS_S_z [rad s^-1]', 'a_RS_S_x [m s^-2]', 'a_RS_S_y [m s^-2]', 'a_RS_S_z [m s^-2]'],
            "Truth" : ['timestamp', 'p_RS_R_x [m]', 'p_RS_R_y [m]', 'p_RS_R_z [m]', 'q_RS_w []', 'q_RS_x []', 'q_RS_y []', 'q_RS_z []', 'v_RS_R_x [m s^-1]', 'v_RS_R_y [m s^-1]', 'v_RS_R_z [m s^-1]', 'b_w_RS_S_x [rad s^-1]', 'b_w_RS_S_y [rad s^-1]', 'b_w_RS_S_z [rad s^-1]', 'b_a_RS_S_x [m s^-2]', 'b_a_RS_S_y [m s^-2]', 'b_a_RS_S_z [m s^-2]'],
            "Vicon" : ['timestamp [ns]', 'p_RS_R_x [m]', 'p_RS_R_y [m]', 'p_RS_R_z [m]', 'q_RS_w []', 'q_RS_x []', 'q_RS_y []', 'q_RS_z []']
        }
    },
    "FCS_XSENS" : {
        "description" : "No description available",
        "headers" : {
            "AHRS" : ["Acc_X", "Acc_Y", "Acc_Z", "Gyr_X", "Gyr_Y", "Gyr_Z", "Quat_q0", "Quat_q1", "Quat_q2", "Quat_q3"]
        }
    }
}
