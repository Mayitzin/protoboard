# -*- coding: utf-8 -*-
"""
Activation Functions
====================

References
----------
.. [AS] Abramowitz, M. et al. (1964) Handbook of Mathematical Functions. NBS
        (http://people.math.sfu.ca/~cbm/aands/abramowitz_and_stegun.pdf)
.. [WP] Press, W. H. et al. (1992) Numerical Recipes in C. Cambridge University
        Press. (http://numerical.recipes/)

See Also
--------
- https://en.wikipedia.org/wiki/Activation_function
- https://ml-cheatsheet.readthedocs.io/en/latest/activation_functions.html

"""

import numpy as np
from .mathfuncs import M_PI, isfloat

__all__ = ['atan', 'bent', 'elliotsig', 'elu', 'gudermannian', 'isru', 'relu',
           'sigmoid', 'softplus', 'tanh']

def atan(x, a=M_PI, b=0.0, c=1.0, d=0.0):
    """
    Return a generalized arctan function of the form:

    .. math::

        \\mathrm{atan}(x) = \\frac{a}{\\pi}\\mathrm{atan}\\big(c(x - d)\\big) + b

    where `a` is a scaling factor, `b` is an offset along Y-axis, and `c` is
    the 'steepness' around the inflection point `d`.

    With default parameters, it becomes an ordinary :math:`\\mathrm{tan}^{-1}`
    function:

    .. math::

        \\mathrm{atan}(x) = \\mathrm{tan}^{-1}(x)

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the atan on.
    a : float
        Scaling factor over the response values.
    b : float
        Offset along the Y-axis.
    c : float
        Steepness around the inflection point.
    d : float
        Offset along the X-axis to apply over the response values. This also
        marks the inflection point of the response.

    Returns
    -------
    y : ndarray
        evaluated values of `x` using the generalized arctan function.

    References
    ----------
    - https://en.wikipedia.org/wiki/Sigmoid_function
    - https://en.wikipedia.org/wiki/Inverse_trigonometric_functions

    """
    if type(x) == list:
        x = np.array(x)
    if a == M_PI:
        return np.arctan(c*(x-d))+b
    return (a/M_PI)*np.arctan(c*(x-d))+b

def bent(x, a=1.0, b=1.0):
    """
    Return a generalized bent function of the form:

    .. math::

        \\mathrm{bent}(x) = ab\\Big(\\sqrt{x^2+\\frac{1}{b^2}}-\\frac{1}{b}+x\\Big)

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the bent on.
    a : float
        Scaling factor over the response values.
    b : float
        Offset along the Y-axis.

    Returns
    -------
    y : ndarray
        evaluated values of `x` using the generalized bent function.

    """
    if type(x) == list:
        x = np.array(x)
    # return a*b*(np.sqrt(x**2+(1.0/b)**2)-(1.0/b)+x)
    return (np.sqrt(x**2+1.0)-1.0)/2.0 + x

def elliotsig(x, a=1.0, b=1.0):
    """
    Return a generalized Elliot Sigmoid function.

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the elliotsig on.
    a : float
        Scaling factor over the return values.
    b : float
        Steepness around the inflection point.

    Returns
    -------
    y : ndarray
        evaluated values of `x` using the generalized elliotsig function.

    """
    if type(x) == list:
        x = np.array(x)
    return b*a*x/(1.0+abs(b*x))

def elu(x, a=1.0, b=1.0):
    """
    Return a generalized exponential linear unit function of the form:

    .. math::

        \\mathrm{elu}(x) = \\left\\{
        \\begin{array}{ll}
            ax       & \\: x \\ge 0 \\\\
            b(e^x-1) & \\: \\mathrm{otherwise}
        \\end{array}
        \\right.

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the ELU on.
    a : float
        Scaling factor over the non-negative input values.
    b : float
        Scaling factor over the negative input values. It is also the lower
        asymptote of the response.

    Returns
    -------
    y : float or ndarray
        evaluated values of `x` using the generalized ELU function.

    References
    ----------
    - https://en.wikipedia.org/wiki/Rectifier_(neural_networks)

    """
    if isfloat(x):
        y = b*(np.exp(x)-1.0) if x < 0.0 else a*x
        return y
    if type(x) == list:
        x = np.array(x)
    # Threshold array and assign its new values
    y = x.copy()
    y_idx = y < 0.0
    y[y_idx] = b*(np.exp(x[y_idx])-1.0)
    if a != 0.0:
        y[~y_idx] = a*x[~y_idx]
    return y

def gudermannian(x, a=M_PI, b=0.0, c=1.0, d=0.0):
    """
    Return a generalized Gudermannian function of the form:

    gudermannian(x) = (a/pi)*arcsin(tanh(c*(x - d)))+b

    where `a` is a scaling factor, `b` is an offset along Y-axis, and `c` is
    the 'steepness' around the inflection point `d`.

    With default parameters, it becomes an ordinary gudermannian function:

    gudermannian(x) = gd(x) = arcsin(tanh(x))

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the sigmoid on.
    a : float
        Scaling factor over the response values.
    b : float
        Offset along the Y-axis.
    c : float
        Steepness around the inflection point.
    d : float
        Offset along the X-axis to apply over the response values. This also
        marks the inflection point of the response.

    Returns
    -------
    y : ndarray
        evaluated values of `x` using the generalized gudermannian function.

    Notes
    -----
    - The function scipy.special.expit also returns the computation of the
      sigmoid function, but without hyper-parametrization.

    References
    ----------
    - https://en.wikipedia.org/wiki/Gudermannian_function
    - https://en.wikipedia.org/wiki/Sigmoid_function
    - https://en.wikipedia.org/wiki/Error_function

    """
    if type(x) == list:
        x = np.array(x)
    return (a/M_PI)*np.arcsin(np.tanh(c*(x-d)))

def isru(x, a=1.0, b=1.0):
    """
    Return a generalized Inverse Square Root Unit function of the form:

    isru(x) = a*x / sqrt(1/b + x^2)

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the ELU on.
    a : float
        Scaling factor over the response values.
    b : float
        Scaling factor over the negative input values. It is also the lower
        asymptote of the response.

    Returns
    -------
    y : float or ndarray
        evaluated values of `x` using the generalized ELU function.

    References
    ----------
    - https://en.wikipedia.org/wiki/Rectifier_(neural_networks)

    """
    if type(x) == list:
        x = np.array(x)
    return a*x/np.sqrt((1/b)+x**2)

def leaky_relu(x, b):
    return relu(x, b=b)

def relu(x, a=1.0, b=0.0):
    """
    Return a generalized rectified linear unit function of the form:

    .. math::

        \\mathrm{relu}(x) = \\left\\{
        \\begin{array}{ll}
            ax & \\: x \\ge 0 \\\\
            bx & \\: \\mathrm{otherwise}
        \\end{array}
        \\right.

    With default parameters it becomes an ordinary ReLU function:

    .. math::

        \\mathrm{ReLu}(x) = \\left\\{
        \\begin{array}{ll}
            x & \\: x \\ge 0 \\\\
            0 & \\: \\mathrm{otherwise}
        \\end{array}
        \\right.

    A positive non-zero value for `b` makes it a Leaky ReLU function:

    .. math::

        \\mathrm{Leaky ReLu}(x) = \\left\\{
        \\begin{array}{ll}
            x  & \\: x \\ge 0 \\\\
            bx & \\: \\mathrm{otherwise}
        \\end{array}
        \\right.

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the ReLU.
    a : float
        Scaling factor over the non-negative input values.
    b : float
        Scaling factor over the negative input values.

    Returns
    -------
    y : float or ndarray
        evaluated values of `x` using the generalized ReLU function.

    References
    ----------
    - https://en.wikipedia.org/wiki/Rectifier_(neural_networks)

    """
    if isfloat(x):
        y = b*x if x < 0.0 else a*x
        return y
    if type(x) == list:
        x = np.array(x)
    # Threshold array and assign its new values
    y = x.copy()
    x_idx = x < 0.0
    if b == 0.0:
        y[x_idx] = 0.0
    else:
        y[x_idx] = b*y[x_idx]
    if a != 1.0:
        y[~x_idx] = a*y[~x_idx]
    return y

def sigmoid(x, a=1.0, b=0.0, c=1.0, d=0.0):
    """
    Return a generalized sigmoid, a.k.a. logistic function, of the form:

    sigmoid(x) = a / (1 + exp(c*(d-x))) + b

    where `a` is a scaling factor, `b` is an offset along Y-axis, and `c` is
    the 'steepness' around the inflection point `d`.

    With default parameters, it becomes an ordinary sigmoid function:

    sigmoid(x) = 1 / (1 + exp(-x))

    The function scipy.special.expit also returns the computation of the sigmoid function, but without hyper-parametrization.

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the sigmoid on.
    a : float
        Scaling factor over the response values.
    b : float
        Offset along the Y-axis.
    c : float
        Steepness around the inflection point.
    d : float
        Offset along the X-axis to apply over the response values. This also
        marks the inflection point of the response.

    Returns
    -------
    y : ndarray
        evaluated values of `x` using the generalized sigmoid function.

    References
    ----------
    .. [DB] Barber, D. (2017) Bayesian Reasoning and Machine Learning. Pages 188-189.
    .. [CB] Bishop, C. (2006) Pattern Recognition and Machine Learning. Springer. Pages 113-114.
    - https://en.wikipedia.org/wiki/Sigmoid_function
    - https://en.wikipedia.org/wiki/Logistic_function
    - https://en.wikipedia.org/wiki/Generalised_logistic_function
    - https://en.wikipedia.org/wiki/Error_function

    Examples
    --------
    >>> dptk.maths.sigmoid(0.0)
    0.5
    >>> import numpy as np
    >>> x = np.linspace(-1, 1, 5)
    >>> x
    array([-1. , -0.5,  0. ,  0.5,  1. ])
    >>> dptk.maths.sigmoid(x)
    array([ 0.26894142,  0.37754067,  0.5       ,  0.62245933,  0.73105858])
    >>> dptk.maths.sigmoid(x, a=2.0, b=-1.0, c=5.0, d=0.5)
    array([-0.99889444, -0.9866143 , -0.84828364,  0.        ,  0.84828364])


    """
    if type(x) == list:
        x = np.array(x)
    return a/(1.0+np.exp(c*(d-x)))+b

def softplus(x, a=0.0, b=0.0, c=1.0):
    """
    Return the values of a generalized softplus function of the form:

    softplus(x) = log(10^a+exp(c*(x-b)))

    With default parameters, it becomes an ordinary softplus function:

    softplus(x) = log(1+exp(x))

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the softplus on.
    a : float
        Lower asymptote of the response.
    b : float
        Offset along the X-axis marking the position of the "knee" point.
    c : float
        Steepness around the "knee" point.

    Returns
    -------
    y : float or ndarray
        evaluated values of `x` using the generalized softplus function.

    References
    ----------
    - https://en.wikipedia.org/wiki/Rectifier_(neural_networks)

    """
    if type(x) == list:
        x = np.array(x)
    return np.log(10.0**a + np.exp(c*(x-b)))

def tanh(x, a=1.0, b=0.0, c=1.0, d=0.0):
    """
    Return a generalized tanh function of the form:

    tanh(x) = a*(tanh(c*(x - d)) + b)

    where `a` is a scaling factor, `b` is an offset along Y-axis, and `c` is
    the 'steepness' around the inflection point `d`.

    With default parameters, it becomes an ordinary tanh function:

    tanh(x) = tanh(x)

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the sigmoid on.
    a : float
        Scaling factor over the response values.
    b : float
        Offset along the Y-axis.
    c : float
        Steepness around the inflection point.
    d : float
        Offset along the X-axis to apply over the response values. This also
        marks the inflection point of the response.

    Returns
    -------
    y : ndarray
        evaluated values of `x` using the generalized tanh function.

    References
    ----------
    .. [DB] Barber, D. (2017) Bayesian Reasoning and Machine Learning. Pg 591.
    .. [CB] Bishop, C. (2006) Pattern Recognition and Machine Learning.
            Springer. Page 245.

    Notes
    -----
    - The function scipy.special.expit also returns the computation of the
      sigmoid function, but without hyper-parametrization.

    References
    ----------
    - https://en.wikipedia.org/wiki/Sigmoid_function
    - https://en.wikipedia.org/wiki/Error_function

    """
    if type(x) == list:
        x = np.array(x)
    return a*(np.tanh(c*(x-d))+b)


