# -*- coding: utf-8 -*-
"""
Common routines
===============

Routines without a fixed home, but roaming in all others.

"""

from .mathfuncs import *
from .activation import *

def print_module_name():
    print("Module: Common")
