# -*- coding: utf-8 -*-
"""
Datasets
========

Functions and methods to load data from the following datasets:

- The EuRoC MAV Dataset (https://projects.asl.ethz.ch/datasets/doku.php?id=kmavvisualinertialdatasets)

"""

import os
import numpy as np
from protoboard.common.mathfuncs import *
from . import datasets_info

ALL_DATASETS = datasets_info.DATASETS

def load_CSV(fname, separator=';'):
    """
    Load lines from a CSV file.

    It exclusively reads files with the extension .csv and returns each line in
    a list of lists.

    Parameters
    ----------
    fname : str
        A file name
    separator : str
        A character used as separator in the file. Default is ';'.

    Returns
    -------
    lines : list
        Values read from the CSV filem as lists of strings.

    Notes
    -----
    This function aims to be a fast reader for simply formatted files and does
    not provide more sophisticated handling of lines with missing values.

    Examples
    --------
    >>> from protoboard.common.datasets import load_CSV
    >>> with open("some_file.csv", 'w') as f: f.write("one;two;three\\nfour;five;six\\n")
    >>> load_CSV('some_file.csv')
    [['one', 'two', 'three'], ['four', 'five', 'six']]
    >>> with open("another_file.csv", 'w') as f: f.write("seven,8,nine\\n10,eleven,12\\n")
    >>> load_CSV('another_file.csv', ',')
    [['seven', '8', 'nine'], ['10', 'eleven', '12']]

    """
    error_msg = "Invalid file: " + str(fname)
    if not fname.endswith(".csv"):
        raise NameError("[ERROR]: " + error_msg)
    lines = []
    try:
        with open(fname, 'r') as f:
            read_lines = f.readlines()
        [lines.append(line.strip().split(separator)) for line in read_lines]
    except FileNotFoundError:
        print("[ERROR]: " + error_msg)
    return lines

def count_alphanumeric_lines(lines=None):
    """
    counts lines that are not entirely numbers (e.g. header lines.)
    """
    if lines is None:
        return 0
    row_count = 0
    if len(lines) > 0:
        for line in lines:
            line_is_float = True
            for element in line:
                line_is_float &= isfloat(element)
            if not line_is_float:
                row_count += 1
            else:
                break
    return row_count

def get_data(path, separator=';', data_type='float'):
    """
    Return the data of a given file.

    Parameters
    ----------
    path : str
        Path to the file (including its name) to load the data from.
    separator : str
        Character used to split the labels
    data_type : str
        Data type used to build the data return array.

    Returns
    -------
    header : list
        List of strings with the labels of the header
    data_array : ndarray
        NumPy array with the numerical contents of file.

    """
    lines = load_CSV(path, separator=separator)
    num_head_lines = count_alphanumeric_lines(lines)
    header_lines = lines[:num_head_lines]
    if num_head_lines == len(lines):
        # print("{} has no numerical data.".format(path))
        return None
    header = get_header(header_lines)
    data_array = np.array(lines[num_head_lines:], dtype=data_type)
    return header, data_array

def get_header(lines):
    """
    Get the split header line from the given lines.
    """
    header = []
    for line in lines:
        new_line = []
        for word in line:
            if word.startswith("#"):
                word = word[1:]
            new_line.append(word.strip())
        header.append(new_line)
    if len(header) < 2:
        header = header[0]
    return header

def list_datasets():
    """
    Return a list of the datasets that can be parsed.
    """
    dataset_list = list(ALL_DATASETS.keys())
    return dataset_list

def show_datasets(verbose=0):
    """
    Show a list of the datasets that can be parsed.
    """
    dataset_list = list_datasets()
    for d in dataset_list:
        print("{}".format(d))
        if verbose > 0:
            data_description = ALL_DATASETS[d]["description"]
            tab_space = 2
            for line in data_description.split("\n"):
                print("{}{}".format(''.join(tab_space*[' ']), line))
        if verbose > 1:
            print("  + Attainable data:")
            for h in list(ALL_DATASETS[d]["headers"].keys()):
                num_labels = len(ALL_DATASETS[d]["headers"][h])
                print("      {} ({} variables)".format(h, num_labels))

def crawl_folder(root_path, file_ext=".csv"):
    csv_files = []
    for path, subdirs, files in os.walk(root_path):
        for name in files:
            if name.endswith(file_ext):
                csv_files.append(os.path.join(path, name))
    return csv_files

def id_separator(filename, ch_list=None):
    """
    Return the character spitting data in a list of strings.

    In certain files, i.e. CSV files, the data is split with a character, which
    normally is a comma, a semi-colon or a tab. This function reads random
    lines of a given file and counts the incidences of a predefined character.
    If this character is repeated the same amount of times, it is returned as
    the identified separator.

    - ToDO: Find a way to make it simpler and faster.

    Parameters
    ----------
    filename : str
        File to analyse.
    ch_list : list
        List of characters to consider in the search.

    Returns
    -------
    separator : str
        The character splitting data in each line.
    """
    # if ch_list is None:
    #     separators = [',', ';', '\t']
    # else:
    separators = ch_list if ch_list is None else [',', ';', '\t']
    # Build a dictionary of the separators
    sep_dict = {s: [] for s in separators}
    with open(filename, 'r') as f:
        read_lines = f.readlines()
    num_lines = len(read_lines)
    # Select random lines
    num_random_samples = 5
    a = np.arange(3, num_lines)  # Discard first three lines of possible header
    np.random.shuffle(a)
    rand_idx = a[:num_random_samples]
    # Count incidences of each character
    for i in rand_idx:
        line = read_lines[i]
        for s in separators:
            c = line.count(s)
            sep_dict[s].append(c)
    # Validate incidences (Each character must appear same number of times per line)
    for s in separators:
        lst = sep_dict[s]
        if not lst or lst.count(lst[0]) == len(lst):
            sep_dict[s] = sep_dict[s][0]
        else:
            sep_dict[s] = 0
    # Select the one with most incidences
    return max(sep_dict, key=sep_dict.get)

def diff_str_lists(s1, s2, ordered=True):
    """
    Compare the elements of two lists of strings and return their difference.

    Parameters
    ----------
    s1 : array-like
        List of strings with labels of the first header to compare.
    s2 : array-like
        List of strings with labels of the second header to compare.
    ordered : bool
        Indicates if the order of elements to compare is important. Default is
        'True'.

    Returns
    -------
    differences : array-like
        List of elements different between headers. Returns an empty list if
        the headers are equal.
    """
    if ordered:
        differences = [i for i, j in zip(s1, s2) if i != j]
    else:
        differences = set(s1).symmetric_difference(s2)
    return differences

def id_header(header, dset_info=None):
    """
    Identify the format of the given header from a list of headers.
    """
    # all_datasets = ALL_DATASETS.keys()
    all_dataset = dset_info.keys() if dset_info is not None else ALL_DATASETS.keys()
    # headers = []
    for d in all_datasets:
        files_headers = list(ALL_DATASETS[d]["headers"].keys())
        for h in files_headers:
            diff = diff_str_lists(ALL_DATASETS[d]["headers"][h], header)
            if len(diff) < 1:
                return (d, h)

class Data:
    """
    This class holds the data and information of a Dataset
    """
    data = {}
    files = []
    def __init__(self, path):
        self.path = path
        if os.path.isfile(self.path):
            separator = id_separator(self.path)
            self.files.append(self.path)
            self.update_data(self.path, separator=separator)
        if os.path.isdir(self.path):
            print("Base folder: {}".format(self.path))
            self.files = crawl_folder(self.path)
            if len(self.files) > 0:
                separator = id_separator(self.files[0])
                for f in self.files:
                    self.update_data(f, separator=separator)

    def update_data(self, filename, separator=';'):
        """
        Update the data dictionary with the given file contents.
        """
        new_data = get_data(filename, separator=separator)
        if new_data is None:
            return None
        new_header, new_array = new_data
        num_cols = np.shape(new_array)[1]
        # Length of header matches given array
        if (len(new_header) - num_cols) != 0:
            print("[ERROR] Number of columns do not correspond to header")
            return None
        _, data_class = id_header(new_header)
        self.data[data_class] = {}
        self.data[data_class]["array"] = new_array
        self.data[data_class]["header"] = new_header

    def show_info(self, v=0):
        """
        Show the information of the read data.
        """
        common_path = os.path.commonpath(self.files)
        print("Common Path: {}".format(common_path))
        data_classes = list(self.data.keys())
        if len(data_classes) > 0:
            print("Data contains arrays of:")
            for c in data_classes:
                m, n = np.shape(self.data[c]["array"])
                print("  {} ({} x {})".format(c, m, n))
                if v > 0:
                    print("    {}".format(" | ".join(self.data[c]["header"])))

