# -*- coding: utf-8 -*-
"""
Essential mathematical routines
===============================

References
----------
.. [Abramowitz] Abramowitz, M. et al. (1964) Handbook of Mathematical Functions. NBS
        (http://people.math.sfu.ca/~cbm/aands/abramowitz_and_stegun.pdf)
.. [Press] Press, W. H. et al. (1992) Numerical Recipes in C. Cambridge University
        Press. (http://numerical.recipes/)
.. [Yano] Yano, M. et al. (2013) Math, Numerics, & Programming (for Mechanical
        Engineers.) MIT. (https://ocw.mit.edu/ans7870/2/2.086/S13/MIT2_086S13_Textbook.pdf)

"""

import numpy as np

__all__ = ['sign', 'isfloat', 'cosd', 'sind', 'skew_matrix', 'd_chord',
           'matrix_diff', 'fit_plane', 'M_PI', 'DEG2RAD', 'RAD2DEG']

M_PI = np.pi
DEG2RAD = M_PI / 180.0
RAD2DEG = 180.0 / M_PI

def sign(x):
    """
    Return the sign of a number.

    .. math::

        \\mathrm{sign}(x) = \\left\\{
        \\begin{array}{ll}
            1 & \\: x \\ge 0 \\\\
            -1 & \\: \\mathrm{otherwise}
        \\end{array}
        \\right.

    Parameters
    ----------
    x : int or float
        Input x.

    Returns
    -------
    y : int
        -1 if x is negative, or 1 otherwise.
    """
    try:
        n = float(x)
    except:
        return None
    s = -1 if n < 0.0 else 1
    return s

def isfloat(x):
    """
    Returns `True` if input string is a float number.

    Parameters
    ----------
    x : str
        Input x.

    Returns
    -------
    y : bool
        The return value, True if `x` is a float value.

    """
    try:
        float(x)
        return True
    except:
        return False

def cosd(x):
    """
    Return the cosine of x, which is expressed in degrees.

    If x is a list, it will be converted first to a NumPy array, and then the
    cosine operation over each value will be carried out.

    Parameters
    ----------
    x : float
        Angle in Degrees

    Returns
    -------
    y : float
        Cosine of given angle

    Examples
    --------
    >>> from protoboard.common.mathfuncs import *
    >>> cosd(0.0)
    1.0
    >>> cosd(90.0)
    0.0
    >>> cosd(-120.0)
    -0.5

    """
    if type(x) == list:
        x = np.asarray(x)
    return np.cos(x*DEG2RAD)

def sind(x):
    """
    Return the sine of x, which is expressed in degrees.

    If x is a list, it will be converted first to a NumPy array, and then the
    sine operation over each value will be carried out.

    Parameters
    ----------
    x : float
        Angle in Degrees

    Returns
    -------
    y : float
        Sine of given angle

    Examples
    --------
    >>> from protoboard.common.mathfuncs import *
    >>> sind(0.0)
    0.0
    >>> sind(90.0)
    1.0
    >>> sind(-120.0)
    -0.86602540378

    """
    if type(x) == list:
        x = np.asarray(x)
    return np.sin(x*DEG2RAD)

def softmax(x):
    """
    Return the values od a softmax function of the form:

    .. math::

        \\mathrm{softmax}(x) = \\frac{1}{\\sum_{i} e^{x_i}}e^x

    The function can also be used from `scipy.special.softmax`.

    Parameters
    ----------
    x : float or ndarray
        Real number (or an array of them) to apply the softmax.

    Returns
    -------
    y : float or ndarray
        Evaluated values of `x` using the generalized softmax function.

    References
    ----------
    .. [W1] https://en.wikipedia.org/wiki/Softmax_function

    """
    if type(x) == list:
        x = np.array(x)
    return np.exp(x)/np.sum(np.exp(x))

def skew_matrix(x):
    """
    Return the 3-by-3 skew-symmetric matrix of a 3-element vector x.

    Parameters
    ----------
    x : array
        3-element array with values to be ordered in a skew-symmetric matrix.

    Returns
    -------
    X : ndarray
        3-by-3 numpy array of the skew-symmetric matrix.

    Examples
    --------
    >>> from protoboard.common.mathfuncs import skew_matrix
    >>> a = [1, 2, 3]
    >>> skew_matrix(a)
    [[ 0. -3.  2.]
     [ 3.  0. -1.]
     [-2.  1.  0.]]
    >>> a = np.array([[4.0], [5.0], [6.0]])
    >>> skew_matrix(a)
    [[ 0. -6.  5.]
     [ 6.  0. -4.]
     [-5.  4.  0.]]

    See Also
    --------
    - https://en.wikipedia.org/wiki/Skew-symmetric_matrix

    """
    if len(x) != 3:
        return None
    return np.array([[0, -x[2], x[1]], [x[2], 0, -x[0]], [-x[1], x[0], 0.0]])

def d_chord(R1, R2):
    """
    Compute the cordal distance between two rotation matrices.

    The chordal distance [RH]_ between two rotation matrices :math:`R_1`,
    :math:`R2` in :math:`SO(3)` is the Euclidean distance between them in the
    embedding space :math:`R^{3\\times 3} = R^9`. Thus,

    .. math::

        \\mathrm{d_{chord}}(R_1, R_2) = \\|R_1 - R_2\\|_F

    where :math:`\\|A\\|_F` is the Frobenius norm of a matrix :math:`A`.

    Possible outputs are:

    .. math::

        \\mathrm{d_{chord}}(R_1, R_2) = \\left\\{
        \\begin{array}{ll}
            0 & \\: \mathrm{if} \\: R_1 = R_2 \\\\
            x \\in (0, \\sqrt[4]{12}] & \\: \mathrm{otherwise}
        \\end{array}
        \\right.

    References
    ----------
    .. [RH] Hartley, R. et al. (2013) Rotation Averaging. International Journal
            of Computer Vision.
            (http://users.cecs.anu.edu.au/~yuchao/files/rotationaveraging-IJCV13.pdf)

    Examples
    --------
    >>> from protoboard.motion import ahrs
    >>> from protoboard.common.mathfuncs import d_chord
    >>> I_3 = ahrs.rotation()
    >>> d_chord(I_3, I_3)
    0.0
    >>> d_chord(I_3, -I_3)
    1.8612097182041991

    """
    A = R1 - R2
    return np.sqrt(np.linalg.norm(A, ord='fro'))

def matrix_diff(X1, X2):
    """
    Compute the difference between two given matrices.

    Use the sum of squared differences between the matrices.
    """
    if (type(X1) != np.ndarray) or (type(X2) != np.ndarray):
        return None
    if np.shape(X1) != np.shape(X2):
        return None
    return np.sum((X1-X2)**2)

def fit_line(points):
    """
    Estimate the line parameters from given 2D or 3D data points.

    TO BE IMPLEMENTED

    Parameters
    ----------
    points : ndarray
        Set of data points

    Returns
    -------
    y : ndarray
        Parameters of line.

    """
    return None

def fit_plane(points, indices=[]):
    """
    Estimate the coefficients of a plane using the given 3D-coordinates.

    By default, it uses all given 3D points to estimate the plane parameters,
    although sub-ranges of points can be specified with an array of indices, so
    that the estimation utilizes certain points only.

    It makes use of a least-squares approximation to solve for the coefficients
    of a plane's equation of the form :math:`ax+by+cz+d=0`

    Parameters
    ----------
    points : array-like
        N-by-3 numPy array with the 3D coordinates of N points.
    indices : array-like
        2D-array with the indices (begining and end) of points to use. The
        default is an empty array (use all points.)

    Returns
    -------
    x : array-like
        The coefficients of the estimated plane.

    References
    ----------
    - https://www.geometrictools.com/Documentation/LeastSquaresFitting.pdf
    - https://ws680.nist.gov/publication/get_pdf.cfm?pub_id=912180
    - http://yadda.icm.edu.pl/yadda/element/bwmeta1.element.baztech-aa1fc635-8bb1-4f47-b962-eaf9d56bac72/c/IM_1-2014-a2.pdf


    """
    x_points, y_points, z_points = [], [], []
    num_indices = len(indices)
    if num_indices < 1:
        x_points = points[:, 0]
        y_points = points[:, 1]
        z_points = np.ones(points.shape[0])
        A = np.c_[x_points, y_points, z_points]
    else:
        for i in range(num_indices):
            idx_1, idx_2 = indices[i][0], indices[i][1]
            x_points = x_points + list(points[idx_1 : idx_2, 0])
            y_points = y_points + list(points[idx_1 : idx_2, 1])
            z_points = z_points + list(points[idx_1 : idx_2, 2])
        num_points = len(x_points)
        x_points = np.array(x_points)[:].reshape((num_points, 1))
        y_points = np.array(y_points)[:].reshape((num_points, 1))
        z_points = np.array(z_points)[:].reshape((num_points, 1))
        A = np.hstack(( x_points, y_points, np.ones(x_points.shape) ))
    # Solve to get plane coefficients
    C,_,_,_ = np.linalg.lstsq(A, z_points, rcond=None)
    return C


def circle(c, radius=1.0, num_points=20):
    """
    Build a circle with the given characteristics.

    Parameters
    ----------
    c : array
        2D Coordinates of center.
    r : float
        Radius of the circle.
    num_points : int
        Number of points.

    Returns
    -------
    points : array
        N-by-2 array with the coordinates of the circle.

    References
    ----------
    .. [WAC] Wolfram Alpha: Circle. (http://mathworld.wolfram.com/Circle.html)

    """
    R = np.linspace(0.0, 2.0*np.pi, num_points+1)
    c_x, c_y = c
    x = c_x + radius*np.cos(R)
    y = c_y + radius*np.sin(R)
    return np.array([x, y]).transpose()


def ellipse(center, phi, axes, num_points=20):
    """
    Build an ellipse with the given characteristics.

    Parameters
    ----------
    center : array
        2D Coordinates of center.
    phi : float
        Angle of the major axis w.r.t. the X-axis
    axes : array
        Lengths of major and minor axes, respectively.
    num_points : int
        Number of points. Defaults to 20.

    Returns
    -------
    points : array
        N-by-2 array with the coordinates of the ellipse.

    References
    ----------
    .. [W1] Wikipedia: https://de.wikipedia.org/wiki/Ellipse#Ellipsengleichung_(Parameterform)
    .. [WAE] Wolfram Alpha: Ellipse. (http://mathworld.wolfram.com/Ellipse.html)

    """
    R = np.linspace(0.0, 2.0*np.pi, num_points+1)
    a, b = axes
    x = center[0] + a*np.cos(R)*np.cos(phi) - b*np.sin(R)*np.sin(phi)
    y = center[1] + a*np.cos(R)*np.sin(phi) + b*np.sin(R)*np.cos(phi)
    return np.array([x, y]).transpose()


def fit_circle(points):
    """
    Return the parameters of the best circle fitting the given points using
    least squares.

    Parameters
    ----------
    points : array
        N-by-2 coordinates of the points.

    Returns
    -------
    center : array
        2D Coordinates of center.
    radius : float
        Radius of the circle.

    References
    ----------
    .. [Gander] W. Gander et al. Least-Squares Fitting of Circles and Ellipses.
      (https://www.emis.de/journals/BBMS/Bulletin/sup962/gander.pdf)

    """
    # coordinates of the barycenter
    x, y = points[:, 0], points[:, 1]
    A = np.hstack((points, np.ones((len(points), 1))))
    p = np.linalg.pinv(A)@np.array([x**2+y**2]).reshape((len(points), 1)).flatten().tolist()
    c_x, c_y = p[:2]/2.0
    center = [c_x, c_y]
    radius = np.sqrt(c_x**2+c_y**2+p[2])
    return center, radius


def fit_ellipse(points):
    """
    Return the parameters of ellipse fitting best the given points.

    This method was developed in 1996 by Andrew Fitzgibbon et al. [AF]_, and
    first implemented in Python (that I know of) by Nicky van Foreest [NV]_ .
    Radim Halir [RH]_ proposes a nummerically stable approach, but is not
    implemented here (yet).

    Parameters
    ----------
    points : array
        N-by-2 coordinates of the points.

    Returns
    -------
    center : array
        2D Coordinates of center.
    phi : float
        Angle of the main axis.
    axes : array
        Lengths of major and minor axes, respectively.

    References
    ----------
    .. [AF] Fitzgibbon A. Direct Linear Squares Fitting of Ellipses. 1996.
      (http://cseweb.ucsd.edu/~mdailey/Face-Coord/ellipse-specific-fitting.pdf)
    .. [NV] van Foreest N. Fitting an Ellipse to a Set of Data Points.
      (http://nicky.vanforeest.com/misc/fitEllipse/fitEllipse.html)
    .. [RH] R. Halir. Numerically stable direct least squares fitting of
      ellipses. (http://autotrace.sourceforge.net/WSCG98.pdf)

    """
    x = points[:, 0, np.newaxis]
    y = points[:, 1, np.newaxis]
    # Design Matrix D
    D = np.hstack((x*x, x*y, y*y, x, y, np.ones_like(x)))
    # Constraint matrix C
    C = np.zeros([6, 6])
    C[0, 2] = 2
    C[2, 0] = 2
    C[1, 1] = -1
    # Eigenvalues and Eigenvectors of (D^T*D)\C
    E, V = np.linalg.eig(np.linalg.inv(D.T@D)@C)
    # E, V = np.linalg.eig(np.dot(np.linalg.inv(np.dot(D.T, D)), C))
    n = np.argmax(np.abs(E))
    p = V[:, n]
    b, c, d, f, g, a = p[1]/2.0, p[2], p[3]/2.0, p[4]/2.0, p[5], p[0]
    num = b**2 - a*c
    der = 2.0*b/(a-c)
    # Semi-Axes
    up = 2.0*(a*f**2 + c*d**2 + g*b**2 - 2.0*b*d*f - a*c*g)
    sqder = np.sqrt(1.0 + der**2)
    div1 = up/(num*(sqder*(c-a) - (c+a)))
    div2 = up/(num*(sqder*(a-c) - (c+a)))
    if any(i < 0.0 for i in [div1, div2]):
        # Fit a circle if any division is negative
        center, radius = fit_circle(points)
        angle = 0.0
        axes = [radius, radius]
    else:
        # CENTER
        x0 = (c*d - b*f)/num
        y0 = (a*f - b*d)/num
        center = np.array([x0, y0])
        # ANGLE OF ROTATION
        angle = 0.5*np.arctan(der)
        # AXES LENGTHS
        axes = np.array([np.sqrt(abs(div1)), np.sqrt(abs(div2))])
    return center, angle, axes


def fit_ellipsoid(points):
    """
    Estimate the ellipsoid parameters from given 3D data points.

    *** TO BE IMPLEMENTED ***

    Parameters
    ----------
    points : ndarray
        Set of data points

    Returns
    -------
    y : ndarray
        Parameters of ellipsoid.

    """
    return None

def dihedral_angle(p1, p2=None):
    """
    Return the angle between two intersecting planes.

    Parameters
    ----------


    References
    ----------
    .. [W2] Wikipedia: Dihedral Angle (https://en.wikipedia.org/wiki/Dihedral_angle)

    """
    if p1 is None:
        return None
    if p2 is None:
        p2 = np.array([1., 1., 0.])
    if np.linalg.norm(p2) == 0.0:
        p2 = np.array([1., 1., 0.])     # Handle NaN
    denom = np.linalg.norm(p1) * np.linalg.norm(p2)
    angle = RAD2DEG*np.arccos(np.dot(p1, p2)/denom)
    if angle > 90.0:
        angle = 180.0 - angle
    return angle

