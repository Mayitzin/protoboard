# -*- coding: utf-8 -*-

# Versioning
MAJOR       = 0
MINOR       = 0
MICRO       = 1
PRE_RELEASE = ''
PKG_VERSION = '{}.{}.{}{}'.format(MAJOR, MINOR, MICRO, PRE_RELEASE)

def pkg_version():
    # return '{}.{}.{}{}'.format(MAJOR, MINOR, MICRO, PRE_RELEASE)
    return PKG_VERSION