# Protoboard

Protobard is a zoo of useful tools for the development of robotic applications using [Python](https://www.python.org/). The included tools are mostly for students, researchers and makers. Optimization is out of scope.

Do you want a specific algorithm for your program? Just import it and use it. A detailed documentation for each functions is also provided (with its corresponding article, if any exists.)

## Guiding principles

* **Python-based**. Modules are set in Python code, which is compact, easier to debug, and allows for ease of extensibility.
* **User friendliness**. Protoboard is designed for humans, following best practices to offer consistent & simple applications, and it minimizes the number of user actions required for common use cases.
* **Easy extensibility**. New modules are simple to add (as new classes and functions), and existing modules provide ample examples, making Protoboard suitable for advanced research too.

## Getting Protoboard

First, clone protoboard using `git`:

```
git clone https://gitlab.com/Mayitzin/protoboard.git
```

Change directory to the protoboard folder:

```
cd protoboard
```

Then, either:

* Run the install command:

```
sudo python setup.py install
```

* or use `pip`:

```
sudo pip install .
```

## Structure

First created as the (now-defuncted) [Robotics](https://github.com/Mayitzin/Robotics) project some years ago, its structure is heavily influenced by the study program of the Master's Degree on [Robotics, Cognition, Intelligence](https://www.in.tum.de/en/for-prospective-students/masters-programs/robotics-cognition-intelligence-msc/curriculum/) of the [TU Munich](https://www.tum.de/).

- Computer Vision.
  - Image Processing.
  - Image Understanding.
- Artificial Intelligence.
  - Computational Intelligence.
  - Machine Learning.
  - Deep Learning.
- Control.
  - Digital Signal Processing.
  - Control Engineering.
  - Digital Control and Embedded Systems.
- Motion.
  - Kinematics.
  - Dynamics.
  - Robotics.
  - Motion Planning.

Check each page or folder for further documentation about the modules, or have a look at the curated list of [Robotics resources](./Resources.md).

See [Gitlab's Markdown](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md) reference for a thorough guide of GitLab Flavored Markdown (GFM) and effectively change this README.
