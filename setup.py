# -*- coding: utf-8 -*-
"""
Protoboard is the core package to prototype Robotics applications with Python.

It provides:

- classes to organize and easily identify large amounts of data.
- useful linear algebra, statistical and arithmetic functions.
- interfacing functions and classes for fast visualizations.
- and much more.

Protoboard is compatible with Python 3 and above. Using an older version is
highly discouraged.

All protoboard wheels distributed on PyPI are BSD licensed.

"""

import sys
from setuptools import setup, find_packages

if sys.version_info.major < 3:
    raise RuntimeError("Python version >= 3 required.")

from versions import pkg_version
protoboard_version = pkg_version()

DEFAULT_URL = 'https://gitlab.com/Mayitzin/protoboard/'

metadata = dict(
    name='protoboard',
    version=protoboard_version,
    description='Python robotics modeling framework and function zoo.',
    long_description=__doc__,
    url=DEFAULT_URL,
    download_url=DEFAULT_URL+'-/archive/master/protoboard-master.zip',
    author='Mario Garcia',
    author_email='mario.garcia@tum.de',
    project_urls={
        "Bug Tracker": DEFAULT_URL+"issues"
    },
    install_requires=['numpy',   # Newest numpy version yields problems
                      'scipy'],
    packages=find_packages()
)

setup(**metadata)
